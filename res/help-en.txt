How to play: 
Use the di­rec­tion­al keys or swipe on the screen to move the tiles. Two tiles with the same num­ber merge into one when they touch. Add them up to reach 2048!
Scores: 
The cur­rent game score is in the right cor­ner, la­belled S. The best score is in the left cor­ner, la­belled B. It's the low­est score of the win­ning games, or the high­est score if no win­ning game has been played.
