import java.io.*;
import javax.microedition.rms.*;

final class HistoryStore {
	private int hiscore;
	private int wins, losses;
	private int secondsPlayed;
	private int moves, merges;

	private static final String STORE_NAME = "history";

	private void resetFields() {
		hiscore = 0;
		wins = losses = 0;
		secondsPlayed = 0;
		moves = merges = 0;
	}

	public void load() throws RecordStoreException {
		final RecordStore rs;
		try {
			rs = RecordStore.openRecordStore(STORE_NAME, false);
		} catch (final RecordStoreNotFoundException ignored) {
			resetFields();
			return;
		}

		resetFields();
		try {
			final RecordEnumeration re = rs.enumerateRecords(null, null, false);
			try {
				while (re.hasNextElement()) {
					final DataInputStream dis = new DataInputStream(
						new ByteArrayInputStream(re.nextRecord()));
					try {
						try {
							dis.readInt(); /* Ignore date */
							setHiscore(dis.readShort());
							secondsPlayed += dis.readShort();
							moves += dis.readShort();
							merges += dis.readShort();
						} finally {
							dis.close();
						}
					} catch (final IOException ioe) {
						ioe.printStackTrace();
					}
				}
			} finally {
				re.destroy();
			}
		} finally {
			rs.closeRecordStore();
		}
	}

	private void setHiscore(int score) {
		final boolean won = score > 0;
		if (score < 0) {
			score = -score;
		}
//		assert score > 0;
		if (wins > 0 ? won && score < hiscore : score > hiscore) {
			hiscore = score;
		}
//		assert hiscore > 0;
		if (won) {
			wins++;
		} else {
			losses++;
		}
	}

	public int getHighScore() {
		return hiscore;
	}

	public int getWins() {
		return wins;
	}

	public int getLosses() {
		return losses;
	}

	public int getMinutesPlayed() {
		return secondsPlayed / 60;
	}

	public int getMoves() {
		return moves;
	}

	public int getMerges() {
		return merges;
	}

	public void add(final GameStat gs)
	throws RecordStoreException, IOException {
		final int score = gs.isWon() ? gs.getScore() : -gs.getScore();

		final ByteArrayOutputStream baos = new ByteArrayOutputStream(16);
		{
			final DataOutputStream dos = new DataOutputStream(baos);
			try {
				dos.writeInt((int) (System.currentTimeMillis() / 60000L));
				dos.writeShort(score);
				dos.writeShort(gs.getSecondsPlayed());
				dos.writeShort(gs.getMoves());
				dos.writeShort(gs.getMerges());
			} finally {
				dos.close();
			}
		}

		final RecordStore rs = RecordStore.openRecordStore(STORE_NAME, true);
		try {
			rs.addRecord(baos.toByteArray(), 0, baos.size());
		} finally {
			rs.closeRecordStore();
		}

		setHiscore(score);
		secondsPlayed += gs.getSecondsPlayed();
		moves += gs.getMoves();
		merges += gs.getMerges();
	}
}
