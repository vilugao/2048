final class GameStat {
	private long startTime, endTime;
	private int moves, merges;
	private int score;
	private boolean won;

	public GameStat() {
	}

	public GameStat(final GameStat gs) {
		this.startTime = gs.startTime;
		this.endTime = gs.endTime;
		this.moves = gs.moves;
		this.merges = gs.merges;
		this.score = gs.score;
		this.won = gs.won;
	}

	public void move() {
//		assert endTime == 0;
		if (startTime == 0) {
			startTime = System.currentTimeMillis();
		}
		moves++;
	}

	public void merge(final int finalBlockNumber) {
//		assert endTime == 0;
		merges++;
		score += finalBlockNumber;
	}

	public void end(final boolean won) {
//		assert startTime != 0;
//		assert endTime == 0;
		endTime = System.currentTimeMillis();
		this.won = won;
	}

	public int getSecondsPlayed() {
//		assert endTime == 0 ? startTime >= 0 : startTime > 0 && startTime <= endTime;
		final long et = endTime == 0 ? System.currentTimeMillis() : endTime;
		return startTime > 0 ? (int) ((et - startTime) / 1000L) : 0;
	}

	public int getMoves() {
		return moves;
	}

	public int getMerges() {
		return merges;
	}

	public int getScore() {
		return score;
	}

	public boolean isOver() {
		return endTime != 0;
	}

	public boolean isWon() {
//		assert endTime != 0;
		return won;
	}
}
