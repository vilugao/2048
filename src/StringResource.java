import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

final class StringResource {
	private final String baseName, locale;
	private final Vector strings = new Vector(8);

	private StringResource(final String baseName, final String locale)
	throws IOException {
		this.baseName = baseName;
		this.locale = locale;

		final InputStream is = getClass()
			.getResourceAsStream("/" + baseName + "-" + locale + ".txt");
		if (is == null) {
			throw new IOException("No resource file found.");
		}
		final InputStreamReader isr = new InputStreamReader(is, "UTF-8");
		try {
			final StringBuffer sb = new StringBuffer(80);
			while (true) {
				final int ch = isr.read();
				if (ch == -1) {
					break;
				} else if (ch == '\n') {
					strings.addElement(sb.toString());
					sb.setLength(0);
				} else if (ch != '\r') {
					sb.append((char) ch);
				}
			}
		} finally {
			isr.close();
		}
	}

	public static StringResource getInstance(
		final String baseName, final String locale
	) throws IOException {
		try {
			return new StringResource(baseName, locale);
		} catch (final IOException ignored) {
		}

		final int hifen = locale.indexOf('-');
		if (hifen == 2) {
			try {
				return new StringResource(baseName, locale.substring(0, hifen));
			} catch (final IOException ignored) {
			}
		}

		return new StringResource(baseName, "en");
	}

	public static StringResource getInstance(final String baseName)
	throws IOException {
		return getInstance(baseName, System.getProperty("microedition.locale"));
	}

	public String getBaseName() {
		return baseName;
	}

	public String getLocale() {
		return locale;
	}

	public String getString(final int index) {
		return (String) strings.elementAt(index);
	}
}
