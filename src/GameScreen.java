import java.util.NoSuchElementException;
import java.util.Random;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.TiledLayer;

final class GameScreen extends Canvas {
	private static final int BACKGROUND_COLOR = 0xfaf8ef;
	private static final int BOX_COLOR = 0xbbada0;
	private static final int EMPTY_BLOCK_COLOR = 0xcdc1b4;
	private static final int DARK_TEXT_COLOR = 0x776e65;
	private static final int LIGHT_TEXT_COLOR = 0xf9f6f2;
	private static final int WHITE_COLOR = 0xffffff;
	private static final int ROUNDED_BOX_SIZE = 4;

	private final TiledLayer tiles;
	private static final int EMPTY_TILE_INDEX = 1;
	private static final int TILE_2_INDEX = 2;
	private static final int TILE_2048_INDEX = 12;
	private Image backgroundImage;

	private static final int BOARD_SIZE = 4;
	private final Random random = new Random(System.currentTimeMillis());
	private int numEmptyCells = BOARD_SIZE * BOARD_SIZE;
	private int directionKey;

	private static final int STATE_RUNNING = 0;
	private static final int STATE_WIN = 1;
	private static final int STATE_OVER = 2;
	private int playState;

	private GameStat statistics = new GameStat();
	private StatListener statListener;
	private int hiScore;
	private final Font scoreFont, boldScoreFont;
	private static final int SCORE_MARGIN = 2;
	/** The score display box width. Includes the horizontal padding of 2 px. */
	private final int scoreBoxWidth;
	private final int hiscoreBoxX;
	private int scoreBoxX;

	private String winMessage, loseMessage;
	private final Font winFont = Font.getFont(
		Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
	private int winMsgPosY;

	public GameScreen() {
		scoreFont = Font.getFont(
			Font.FACE_MONOSPACE, Font.STYLE_PLAIN, Font.SIZE_SMALL);
		boldScoreFont = Font.getFont(
			scoreFont.getFace(), Font.STYLE_BOLD, scoreFont.getSize());
		scoreBoxWidth = 5 * boldScoreFont.charWidth('0') + 2 * SCORE_MARGIN;
		hiscoreBoxX = SCORE_MARGIN + scoreFont.charWidth('H') + 1;

		final Font numFont = Font.getFont(
			Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_SMALL);
		final int tileWidth = ((numFont.stringWidth("2048") + 4) / 8 + 1) * 8;
		tiles = new TiledLayer(BOARD_SIZE, BOARD_SIZE,
			getTiledImage(numFont, tileWidth), tileWidth, tileWidth);
		tiles.fillCells(0, 0, BOARD_SIZE, BOARD_SIZE, EMPTY_TILE_INDEX);

		/**/
		spawnBlock();
		spawnBlock();
		/*/ // Generate all tiles for testing.
		for (int y = 0; y < BOARD_SIZE; y++) {
			for (int x = 0; x < BOARD_SIZE; x++) {
				tiles.setCell(x, y,
					Math.min(TILE_2048_INDEX - 1,
						BOARD_SIZE * (y % 3) + x + TILE_2_INDEX));
			}
		}
		numEmptyCells = 0;
		//*/
	}

	private Image getTiledImage(final Font f, final int tileWidth) {
		final int cols = 4, rows = 3;
		final Image img = Image.createImage(cols * tileWidth, rows * tileWidth);
		final Graphics g = img.getGraphics();
		g.setColor(BOX_COLOR);
		g.fillRect(0, 0, img.getWidth(), img.getHeight());
		g.setFont(f);
		final int[] colour = {
			EMPTY_BLOCK_COLOR, 0xeee4da, 0xede0c8, 0xf2b179,
			0xf59563, 0xf67c5f, 0xf65e3b, 0xedcf72,
			0xedcc61, 0xedc850, 0xedc53f, 0xedc22e,
		};
		final int numX = tileWidth / 2;
		final int numY = (tileWidth - f.getHeight()) / 2;
		for (int i = 0; i < cols * rows; i++) {
			g.setColor(colour[i]);
			g.fillRoundRect(2, 2, tileWidth - 4, tileWidth - 4,
				ROUNDED_BOX_SIZE, ROUNDED_BOX_SIZE);
			if (i != EMPTY_TILE_INDEX - 1) {
				g.setColor(i >= 3 ? LIGHT_TEXT_COLOR : DARK_TEXT_COLOR);
				g.drawString(Integer.toString(1 << i), numX, numY,
					Graphics.HCENTER | Graphics.TOP);
			}
			if (i % cols == cols - 1) {
				g.translate(-g.getTranslateX(), tileWidth);
			} else {
				g.translate(tileWidth, 0);
			}
		}
		return img;
	}

	public void setWinMessage(final String msg) {
		winMessage = msg;
	}

	public void setLoseMessage(final String msg) {
		loseMessage = msg;
	}

	public void setHighScore(final int hi) {
		hiScore = hi;
	}

	public void setGameStatListener(final StatListener l) {
		statListener = l;
	}

	protected void showNotify() {
		if (scoreBoxX == 0) {
			resizeTo(getWidth(), getHeight());
		}
	}

	protected void sizeChanged(final int w, final int h) {
		super.sizeChanged(w, h);
		resizeTo(w, h);
		repaint();
	}

	private void resizeTo(final int w, final int h) {
		scoreBoxX = w - scoreBoxWidth - SCORE_MARGIN;

		final int x = (w - tiles.getWidth()) / 2;
		final int y = Math.max(
			(h - tiles.getHeight()) / 2,
			boldScoreFont.getHeight() + 2 * SCORE_MARGIN);
		tiles.setPosition(x, y);

		winMsgPosY = (h - winFont.getHeight() + y + tiles.getHeight()) / 2;

		if (backgroundImage == null
			|| backgroundImage.getHeight() != h
			|| backgroundImage.getWidth() != w
		) {
			backgroundImage = Image.createImage(w, h);
		}
		final Graphics g = backgroundImage.getGraphics();

		g.setColor(BACKGROUND_COLOR);
		g.fillRect(0, 0, w, h);
		g.setFont(scoreFont);

		g.setColor(DARK_TEXT_COLOR);
		g.drawChar('B', hiscoreBoxX - 2, SCORE_MARGIN,
			Graphics.RIGHT | Graphics.TOP);
		g.drawChar('S', scoreBoxX - 2, SCORE_MARGIN,
			Graphics.RIGHT | Graphics.TOP);

		g.setColor(BOX_COLOR);
		g.fillRoundRect(hiscoreBoxX, SCORE_MARGIN,
			scoreBoxWidth, scoreFont.getHeight(),
			ROUNDED_BOX_SIZE, ROUNDED_BOX_SIZE);
		g.fillRoundRect(scoreBoxX, SCORE_MARGIN,
			scoreBoxWidth, scoreFont.getHeight(),
			ROUNDED_BOX_SIZE, ROUNDED_BOX_SIZE);

		g.fillRoundRect(x - 1, y - 1,
			tiles.getWidth() + 2, tiles.getHeight() + 2,
			ROUNDED_BOX_SIZE, ROUNDED_BOX_SIZE);
	}

	public void paint(final Graphics g) {
		g.drawImage(backgroundImage, 0, 0, Graphics.LEFT | Graphics.TOP);

		g.setColor(WHITE_COLOR);
		g.setFont(boldScoreFont);
		/* High score */
		if (hiScore != 0) {
			g.drawString(Integer.toString(hiScore),
				hiscoreBoxX + scoreBoxWidth - SCORE_MARGIN, SCORE_MARGIN,
				Graphics.RIGHT | Graphics.TOP);
		}
		/* Game score */
		g.drawString(Integer.toString(statistics.getScore()),
			scoreBoxX + scoreBoxWidth - SCORE_MARGIN, SCORE_MARGIN,
			Graphics.RIGHT | Graphics.TOP);

		tiles.paint(g);

		if (playState == STATE_WIN) {
			g.setColor(DARK_TEXT_COLOR);
			g.setFont(winFont);
			g.drawString(winMessage, getWidth() / 2, winMsgPosY,
				Graphics.HCENTER | Graphics.TOP);
		} else if (playState == STATE_OVER) {
			g.setColor(DARK_TEXT_COLOR);
			g.setFont(winFont);
			g.drawString(loseMessage, getWidth() / 2, winMsgPosY,
				Graphics.HCENTER | Graphics.TOP);
		}
	}

	public void resetGame() {
		statistics = new GameStat();
		directionKey = 0;
		tiles.fillCells(0, 0, BOARD_SIZE, BOARD_SIZE, EMPTY_TILE_INDEX);
		numEmptyCells = BOARD_SIZE * BOARD_SIZE;
		playState = STATE_RUNNING;
		spawnBlock();
		spawnBlock();
		repaint();
	}

	private static final int NINE_TENTHS = (int) ((~0 >>> 1) * 9L / 10);

	private void spawnBlock() {
		int i = (random.nextInt() >>> 1) % numEmptyCells;
		for (int y = 0; y < BOARD_SIZE; y++) {
			for (int x = 0; x < BOARD_SIZE; x++) {
				if (tiles.getCell(x, y) != EMPTY_TILE_INDEX) {
					continue;
				}
				if (i == 0) {
					final int tileIndex = (random.nextInt() >>> 1) < NINE_TENTHS
						? TILE_2_INDEX : TILE_2_INDEX + 1;
					tiles.setCell(x, y, tileIndex);
					numEmptyCells--;
					return;
				}
				i--;
			}
		}
		throw new NoSuchElementException();
	}

	protected void keyReleased(final int keyCode) {
		final int gameAction = getGameAction(keyCode);
		switch (gameAction) {
		case UP:
		case LEFT:
		case RIGHT:
		case DOWN:
			directionKey = gameAction;
			moveBlocks();
			break;
		default:
			break;
		}
	}

	private int pointerOrigX, pointerOrigY;
	private static final int DRAG_SLOPPINESS_LIMIT = 2;

	protected void pointerPressed(final int x, final int y) {
		pointerOrigX = x;
		pointerOrigY = y;
	}

	protected void pointerReleased(final int x, final int y) {
		final int dx = x - pointerOrigX, dy = y - pointerOrigY;
		if (Math.abs(dx) > DRAG_SLOPPINESS_LIMIT * Math.abs(dy)) {
			directionKey = dx < 0 ? LEFT : RIGHT;
			moveBlocks();
		} else if (Math.abs(dy) > DRAG_SLOPPINESS_LIMIT * Math.abs(dx)) {
			directionKey = dy < 0 ? UP : DOWN;
			moveBlocks();
		}
	}

	private void moveBlocks() {
		if (playState != STATE_RUNNING) {
			return;
		}
		boolean moved = false;

		for (int j = 0; j < BOARD_SIZE; j++) {
			for (int k = 0; k < BOARD_SIZE - 1; k++) {
				boolean hasCell = false;
				for (int i = k + 1; i < BOARD_SIZE; i++) {
					final int cellI = getTileIndexAt(i, j);
					if (cellI == EMPTY_TILE_INDEX) {
						continue; /* Search the next occupied 2nd cell. */
					}
					hasCell = true;
					final int cellK = getTileIndexAt(k, j);
					if (cellK == EMPTY_TILE_INDEX) {
						/* First cell is empty: pull next cell. */
						mergeCells(k, i, j, cellI);
						moved = true;
						continue;
					} else if (cellK == cellI) { /* Join the same next cell. */
						mergeCells(k, i, j, cellI + 1);
						numEmptyCells++;
						statistics.merge(4 << (cellI - TILE_2_INDEX));
						if (cellI == TILE_2048_INDEX - 1) {
							playState = STATE_WIN;
							statistics.end(true);
							if (statListener != null) {
								statListener.gameEnd(new GameStat(statistics));
							}
						}
						moved = true;
					}
					break;
				}
				if (!hasCell) {
					break;
				}
			}
		}

		if (moved) {
			statistics.move();
			spawnBlock();
			checkOver();
			repaint();
		}
	}

	private void checkOver() {
		if (numEmptyCells > 0) {
			return;
		}
		for (int y = 0; y < BOARD_SIZE; y++) {
			for (int x = 1; x < BOARD_SIZE; x++) {
				if (tiles.getCell(x - 1, y) == tiles.getCell(x, y)) {
					return;
				}
				if (tiles.getCell(y, x - 1) == tiles.getCell(y, x)) {
					return;
				}
			}
		}
		playState = STATE_OVER;
		statistics.end(false);
		if (statListener != null) {
			statListener.gameEnd(new GameStat(statistics));
		}
	}

	private int getTileIndexAt(int x, final int y) {
		switch (directionKey) {
		case RIGHT:
			x = BOARD_SIZE - 1 - x;
			/* fall-through */
		case LEFT:
			return tiles.getCell(x, y);

		case DOWN:
			x = BOARD_SIZE - 1 - x;
			/* fall-through */
		case UP:
			return tiles.getCell(y, x);

		default:
			throw new IllegalArgumentException();
		}
	}

	private void mergeCells(
		int destX, int srcX, final int y, final int newTileIndex
	) {
		switch (directionKey) {
		case RIGHT:
			destX = BOARD_SIZE - 1 - destX;
			srcX = BOARD_SIZE - 1 - srcX;
			/* fall-through */
		case LEFT:
			tiles.setCell(srcX, y, EMPTY_TILE_INDEX);
			tiles.setCell(destX, y, newTileIndex);
			break;

		case DOWN:
			destX = BOARD_SIZE - 1 - destX;
			srcX = BOARD_SIZE - 1 - srcX;
			/* fall-through */
		case UP:
			tiles.setCell(y, srcX, EMPTY_TILE_INDEX);
			tiles.setCell(y, destX, newTileIndex);
			break;

		default:
			throw new IllegalArgumentException();
		}
	}

	public GameStat getGameStat() {
		return new GameStat(statistics);
	}

	public boolean isGameRunning() {
		return statistics.getScore() > 0 && playState == STATE_RUNNING;
	}
}
