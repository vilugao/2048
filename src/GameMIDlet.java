import java.io.IOException;

import javax.microedition.lcdui.*;
import javax.microedition.midlet.MIDlet;
import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreFullException;

public final class GameMIDlet extends MIDlet
implements CommandListener, StatListener {
	private static final int R_COMMAND_NEW = 0;
	private static final int R_COMMAND_NEW_GAME = 1;
	private static final int R_COMMAND_STATS = 2;
	private static final int R_COMMAND_HELP = 3;
	private static final int R_COMMAND_BACK = 4;
	private static final int R_COMMAND_EXIT = 5;
	private static final int R_PROGRESS_TITLE = 6;
	private static final int R_PROGRESS_QUESTION = 7;
	private static final int R_COMMAND_CANCEL = 8;
	private static final int R_YOU_WIN = 9;
	private static final int R_YOU_LOSE = 10;

	private final HistoryStore history = new HistoryStore();
	private final StringResource statsResource
		= StringResource.getInstance("stats");
	private Form statsForm;

	private final Command newGameCommand, exitCommand;
	private final Command statsCommand, helpCommand, backCommand;
	private final GameScreen gameScreen = new GameScreen();
	private boolean statsIncluded;

	private final Command cancelCommand, okNewGameCommand, okExitCommand;
	private final Alert gameProgressAlert;

	private Displayable helpScreen;

	public GameMIDlet() throws IOException {
		final StringResource res = StringResource.getInstance("strings");

		newGameCommand = new Command(
			res.getString(R_COMMAND_NEW), res.getString(R_COMMAND_NEW_GAME),
			Command.SCREEN, 0);
		statsCommand = new Command(res.getString(R_COMMAND_STATS),
			Command.SCREEN, 1);
		helpCommand = new Command(res.getString(R_COMMAND_HELP),
			Command.HELP, 2);
		exitCommand = new Command(res.getString(R_COMMAND_EXIT),
			Command.EXIT, 3);

		backCommand = new Command(res.getString(R_COMMAND_BACK),
			Command.BACK, 1);

		gameScreen.setWinMessage(res.getString(R_YOU_WIN));
		gameScreen.setLoseMessage(res.getString(R_YOU_LOSE));
		gameScreen.addCommand(exitCommand);
		gameScreen.addCommand(newGameCommand);
		gameScreen.addCommand(statsCommand);
		gameScreen.addCommand(helpCommand);
		gameScreen.setCommandListener(this);

		cancelCommand = new Command(res.getString(R_COMMAND_CANCEL),
			Command.CANCEL, 0);
		okNewGameCommand = new Command(newGameCommand.getLabel(),
			Command.OK, 0);
		okExitCommand = new Command(exitCommand.getLabel(),
			Command.OK, 0);

		gameProgressAlert = new Alert(res.getString(R_PROGRESS_TITLE),
			res.getString(R_PROGRESS_QUESTION),
			null, AlertType.WARNING);
		gameProgressAlert.setTimeout(Alert.FOREVER);
		gameProgressAlert.addCommand(cancelCommand);
		gameProgressAlert.setCommandListener(this);
	}

	public void startApp() {
		try {
			history.load();
		} catch (final RecordStoreException rse) {
			final Alert a = new Alert("Error",
				"Couldn't load the game history. "
				+ "This feature will be disabled.",
				null, AlertType.WARNING);
			Display.getDisplay(this).setCurrent(a, gameScreen);
			return;
		}
		gameScreen.setHighScore(history.getHighScore());
		gameScreen.setGameStatListener(this);
		Display.getDisplay(this).setCurrent(gameScreen);
	}

	public void pauseApp() {}

	public void destroyApp(final boolean uncondicional) {}

	public void commandAction(final Command c, final Displayable d) {
		if (c == helpCommand) {
			try {
				Display.getDisplay(this).setCurrent(getHelpScreen());
			} catch (final IOException ioe) {
				ioe.printStackTrace();
			}
			return;
		} else if (c == statsCommand) {
			updateStatsScreen();
			Display.getDisplay(this).setCurrent(statsForm);
			return;
		} else if (c == backCommand) {
			Display.getDisplay(this).setCurrent(gameScreen);
			return;
		}

		if (d == gameScreen && gameScreen.isGameRunning()) {
			gameProgressAlert.removeCommand(
				c == newGameCommand ? okExitCommand : okNewGameCommand);
			gameProgressAlert.addCommand(
				c == newGameCommand ? okNewGameCommand : okExitCommand);
			Display.getDisplay(this).setCurrent(gameProgressAlert);
			return;
		}
		if (d == gameProgressAlert) {
			Display.getDisplay(this).setCurrent(gameScreen);
		}
		if (c == exitCommand || c == okExitCommand) {
			destroyApp(true);
			notifyDestroyed();
		} else if (c == newGameCommand || c == okNewGameCommand) {
			gameScreen.resetGame();
			statsIncluded = false;
		}
	}

	public void gameEnd(final GameStat gs) {
		try {
			history.add(gs);
			statsIncluded = true;
			gameScreen.setHighScore(history.getHighScore());
		} catch (final RecordStoreFullException rsfe) {
			final Alert a = new Alert("Error",
				"Couldn't save due to game history full.",
				null, AlertType.WARNING);
			a.setTimeout(Alert.FOREVER);
			Display.getDisplay(this).setCurrent(a, gameScreen);
		} catch (final RecordStoreException rse) {
			final Alert a = new Alert("Error",
				"Couldn't save this game to history.",
				null, AlertType.WARNING);
			Display.getDisplay(this).setCurrent(a, gameScreen);
		} catch (final IOException ioe) {
			final Alert a = new Alert("Error",
				"Couldn't save this game to history.",
				null, AlertType.WARNING);
			Display.getDisplay(this).setCurrent(a, gameScreen);
		}
	}

	private Displayable getHelpScreen() throws IOException {
		if (helpScreen == null) {
			final StringResource res = StringResource.getInstance("help");
			final Form f = new Form(helpCommand.getLabel());
			f.append(new StringItem(res.getString(0), res.getString(1)));
			f.append(new StringItem(res.getString(2), res.getString(3)));
			f.addCommand(backCommand);
			f.setCommandListener(this);
			helpScreen = f;
		}
		return helpScreen;
	}

	private void updateStatsScreen() {
		final int wins = history.getWins();
		final int losses = history.getLosses();
		final String minsPlayed, moves, merges;
		{
			GameStat gs = gameScreen.getGameStat();
			if (statsIncluded && gs.isOver()) {
				gs = new GameStat();
			}
			minsPlayed = String.valueOf(history.getMinutesPlayed()
				+ gs.getSecondsPlayed() / 60);
			moves = String.valueOf(history.getMoves()
				+ gs.getMoves());
			merges = String.valueOf(history.getMerges()
				+ gs.getMerges());
		}

		if (statsForm == null) {
			statsForm = new Form(statsCommand.getLabel());
			statsForm.append(new StringItem(statsResource.getString(0),
				String.valueOf(wins + losses)));
			statsForm.append(new StringItem(statsResource.getString(1),
				String.valueOf(wins)));
			statsForm.append(new StringItem(statsResource.getString(2),
				String.valueOf(losses)));
			statsForm.append(new StringItem(statsResource.getString(3),
				minsPlayed));
			statsForm.append(new StringItem(statsResource.getString(4),
				moves));
			statsForm.append(new StringItem(statsResource.getString(5),
				merges));

			statsForm.addCommand(backCommand);
			statsForm.setCommandListener(this);
		} else {
			((StringItem) statsForm.get(0)).setText(
				String.valueOf(wins + losses));
			((StringItem) statsForm.get(1)).setText(String.valueOf(wins));
			((StringItem) statsForm.get(2)).setText(String.valueOf(losses));
			((StringItem) statsForm.get(3)).setText(minsPlayed);
			((StringItem) statsForm.get(4)).setText(moves);
			((StringItem) statsForm.get(5)).setText(merges);
		}
	}
}
