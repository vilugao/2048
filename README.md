2048
====

This code repository is a 2048 game clone for Java ME platform. The game is
entirely written by Vinícius Lugão. It is based on the original game rules from
[Gabriele Cirulli version](https://github.com/gabrielecirulli/2048).

This game app has three available languages: English (default), Portuguese and
Spanish.

Screenshots
-----------

![Screenshot of the 2048 game running on Nokia X3-02 phones.](screen/nokia-x302.png)
![Screenshot of the 2048 game running on Nokia Asha phones.](screen/nokia-asha.png)
![Screenshot of the 2048 game stats running on Nokia Asha phones.](screen/nokia-asha-stats.png)

System requirements
-------------------

This game app runs on devices that meets the minimum specifications below:

 *  Configuration: CLDC 1.0
 *  Profile: MIDP 2.0 or 2.1
 *  Screen resolution: the standard 240 × 320 is the minimum recommended.
    The game is unplayable for phones with 128 × 128 screen or below.
 *  Memory: at least 32 kB free.
 *  Directional keypads or touch enabled screens.

Download
--------

This Java ME game is available to be downloaded at
[https://bitbucket.org/vilugao/2048/downloads/](https://bitbucket.org/vilugao/2048/downloads/).

Compiling the source codes
--------------------------

If you want to build, there is a _MANIFEST.MF_ file and a prepared _.jad_ file
in the _bin_ directory. The _src_ directory contains the Java source codes. The
_res_ directory contains only the resource files that should be packaged into a
_.jar_ file with all the preverified compiled classes.

The _Sun's Wireless Toolkit 2.5.2_ is used to build this game app.
